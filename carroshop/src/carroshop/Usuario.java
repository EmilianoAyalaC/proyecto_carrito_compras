package carroshop;

public class Usuario {
  
  String nombre;
  String password;
  float    saldo;
  String membresia;
  float  descuento;  
  
    
 public Usuario(){}
 
 public Usuario(String nombre, String password, float saldo, String membresia, float descuento){
   
   this.nombre = nombre;
   this.password = password;
   this.saldo = saldo;
   this.membresia = membresia;
   this.descuento = descuento;
 }
 public String getNombre() {
   return nombre;
 }
 
 public void setNombre(String nombre) {
   this.nombre = nombre;
 }
       
 public String getPassword(){
   return password; 
 }
 
 public void setPassword(String password){
   this.password = password;  
 }
 
 public float getSaldo(){
   return saldo;  
 }
 
 public void setSaldo(float saldo){
   this.saldo = saldo;
 }
 
 public String getMembresia(){
   return membresia;  
 }
 
 public void setMembresia(String membresia){
   this.membresia = membresia;
 }
 
 public float getDescuento(){
   return descuento; 
 }
 
 public void setDescuento(float descuento){
   this.descuento = descuento; 
 }
 
}
