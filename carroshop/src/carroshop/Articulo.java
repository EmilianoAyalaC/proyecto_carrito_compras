
package carroshop;


public class Articulo {

  private String nombre;
  private int    cantidad;
  private float  costo;
  
  public Articulo(){
      
  }
  
  public Articulo(String nombre, int cantidad, float costo){
    this.nombre = nombre;
    this.cantidad = cantidad;
    this.costo = costo;
      
  }
  
  public Articulo(int cantidad, float costo){
     this.cantidad = cantidad;
     this.costo = costo;
  }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
  
 
  
  
    
    
}
